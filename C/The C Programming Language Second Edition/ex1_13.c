#include <stdio.h>

#define IN 1
#define OUT 0

/*  Barchart of word length */
int main (){

  int c, i, w;
  int state;
  state = OUT;

  int nlength[10];
  for (i = 0; i < 10; ++i)
      nlength[i] = 0;

  while ((c = getchar()) != EOF){
      if (c !=  ' ' && c != '\t' && c != '\n'){
          if (state == OUT){
              state = IN;
              w = 1;
          }
          else
             ++w;
      }
      else if (state == IN){
          state = OUT;
          ++nlength[w];
      }
  }
  for (i = 1; i < 10; ++i) {
      printf("Words with %d characters: ", i);
      for (int j = 0; j < nlength[i]; ++j)
          printf("-");
      printf("\n");
  }

}
