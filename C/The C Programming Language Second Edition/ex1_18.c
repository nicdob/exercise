#include <stdio.h>

/*  remove trailing blanks and tabs from each line of input, delete blank lines
 *  delete whitespace backwards from \n till first non-whitespace,
 *  if only remaining char is \n, remove it
 */


#define MAXLENGTH 1000

int getloine(char line[], int maxline);
void rmtrail(char line[], int length);
void pulltail(char line[], int dest);

int main()
{
    char line [MAXLENGTH]; // current input line
    int length;
    while ((length = getloine(line, MAXLENGTH)) > 0){
        /* if (line[-1] != '\n'){ */
            /* line[] */
        /* } */
        rmtrail(line, length);
    }
}

void rmtrail(char line[], int length)
{
    for (int l = length; line[l-2] >= 0; --l){
        if (line[l-2] == ' ' || line[l-2] == '\t')
            pulltail(line, l-2);
        else
            return;
        if (line[0] == '\n')
            line[0] = '\0';
    }
}

void pulltail(char line[], int dest)
{
    for (int i = dest; line[i] != '\0' && i+1 <= MAXLENGTH; i++)
        line[dest] = line[dest + 1];
}

int getloine(char line[], int maxline)
{
    int i, c;
    for (i = 0; i < maxline-1 && (c = getchar()) != EOF && c != '\n'; ++i)
        line[i] = c;
    if (c == '\n') {
        line[i] = c;
        ++i;
    }
    line[i] = '\0';
    return i;
}
