#include <stdio.h>

// replaces chains of multiple blanks with a single blank
int main()
{
    int blanks = 0;
    int c;
    while ((c = getchar()) != EOF)
    {
        if (c != ' ')
            blanks = 0;
        if (c == ' ')
            blanks++;
        if (blanks < 2)
            putchar(c);
    }
}
