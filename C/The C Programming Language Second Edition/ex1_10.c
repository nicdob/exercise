#include <stdio.h>

// replace backspaces with \b, tabs with \t and backslashes with double backslashes
int main()
{
    int c;
    while ((c = getchar()) != EOF)
    {
        if (c == '\b')
        {
            putchar('\\');
            c = 'b';
        }
        if (c == '\t')
        {
            putchar('\\');
            c = 't';
        }
        if (c == '\\')
        {
            c = '\\';
        }
        putchar(c);
    }
}
