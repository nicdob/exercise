#include <stdio.h>

// Count tabs, spaces and newlines of input
int main()
{
    int c;
    double tabs, spaces, lines = 0;
    while ((c = getchar()) != EOF)
    {
        if (c == '\t')
            tabs++;
        if (c == ' ')
            spaces++;
        if (c == '\n')
            lines++;
    }
    printf("Tabs: %.0f Spaces: %.0f Newlines: %.0f", tabs, spaces, lines);
}
