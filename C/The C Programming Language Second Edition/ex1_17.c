#include <stdio.h>

/*  Print all input lines longer than 80 characters */

#define MAXLENGTH 1000

int getloine(char line[], int maxline);
void copy(char to[], char from[]);

int main() {
    int len; // current line length
    char line[MAXLENGTH]; // current input line, with max length (array size)
    while ((len = getloine(line, MAXLENGTH)) > 0){
       if (len > 80)
           printf("%s", line);
    }
    return 0;
}

void copy(char to[], char from[]){
    int i;
    i = 0;
    while ((to[i] = from[i]) != '\0')
        ++i;
}

int getloine (char line[], int maxline) {
    int i, c;

    for (i = 0; i < maxline-1 && (c = getchar()) != EOF && c != '\n'; ++i)
        line[i] = c;
    if (c == '\n') {
        line[i] = c;
        ++i;
    }
    line[i] = '\0';
    return i;
}
