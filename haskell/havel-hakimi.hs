module Main where
import Data.List
import Data.Ord

example = [5, 3, 0, 2, 6, 2, 0, 7, 2, 5]

-- Havel-Hakimi algorithm
-- list empty -> true
-- remove all 0s
-- sort in descending order
-- remove largest/first member N
-- if removed N > length of remaining list -> false
-- subtract 1 from first N elements of remaining list
-- loop

subfirstN :: Int -> [Int] -> [Int]
subfirstN n list = [ x - 1 | x <- take n list ] ++ drop n list

havhak :: [Int] -> Bool
havhak [] = True
havhak [0] = True  -- needed, since list comprehension in line 25 wouldn't work
havhak ab
    | x > length xs = False
    | otherwise = havhak $ subfirstN x xs
    where x:xs = sortOn Down [ y | y <- ab, y/=0 ]


main =
    putStrLn result
    where result = if havhak example then "True" else "False"
