module Main where
  import Data.List
  import Data.Array

  -- Morse code exercise from
  -- https://old.reddit.com/r/dailyprogrammer/comments/cmd1hb/20190805_challenge_380_easy_smooshed_morse_code_1/
  -- implement smorse, which converts a string into morse code without spaces (smooshed morse)
  --
  -- Part 2: https://old.reddit.com/r/dailyprogrammer/comments/cn6gz5/20190807_challenge_380_intermediate_smooshed/
  -- implement smalpha, which given a permutation of the alphabet encoded in
  -- smooshed morse code, returns a possible decoding back.
  -- examples (multiple decodings possible):
  -- smalpha(".--...-.-.-.....-.--........----.-.-..---.---.--.--.-.-....-..-...-.---..--.----..")
  --  => "wirnbfzehatqlojpgcvusyxkmd"
  -- smalpha(".----...---.-....--.-........-----....--.-..-.-..--.--...--..-.---.--..-.-...--..-")
  --  => "wzjlepdsvothqfxkbgrmyicuna"
  -- smalpha("..-...-..-....--.---.---.---..-..--....-.....-..-.--.-.-.--.-..--.--..--.----..-..")
  --  => "uvfsqmjazxthbidyrkcwegponl"
  --

  morse = ".- -... -.-. -.. . ..-. --. .... .. .--- -.- .-.. -- -. --- .--. --.- .-. ... - ..- ...- .-- -..- -.-- --.."
  letters = ['a'..'z']


  -- Exercise 1
  dictMorse = listArray ('a','z') $ words morse

  smorse :: [Char] -> [Char]
  smorse c = foldr (++) [] $ map (\x -> dictMorse ! x) $ filter (/= ' ') c


  -- Exercise 2
  -- idea: implement a dichotomic search tree (https://en.wikipedia.org/wiki/Dichotomic_search)
  -- with nodes having values, to recursively search for a solution.
  -- depth-first, post-order (https://en.wikipedia.org/wiki/Tree_traversal#Depth-first_search_of_binary_tree)
  data DichTree a = Empty | Node a (DichTree a) (DichTree a)
    deriving (Show, Eq)

  leaf x = Node x Empty Empty

  tree = Node 'a' (Node 'b' (leaf 'd')
     (leaf 'e'))
   (Node 'c' Empty
    (Node 'f' (leaf 'g')
     Empty))

  --dichTreeFromList ::
  --smalpha :: [Char] -> [Char]
  --smalpha a =


  -- END
  main =
    putStrLn "Void"
